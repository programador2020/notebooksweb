-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 12-06-2020 a las 17:28:09
-- Versión del servidor: 10.1.37-MariaDB-0+deb9u1
-- Versión de PHP: 7.0.33-0+deb9u7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `nataliaCine`
--
CREATE DATABASE IF NOT EXISTS `nataliaCine` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `nataliaCine`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `candy`
--

CREATE TABLE `candy` (
  `candy_id` int(10) NOT NULL,
  `candy_nombr` varchar(100) NOT NULL,
  `candy_precio` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `candy`
--

INSERT INTO `candy` (`candy_id`, `candy_nombr`, `candy_precio`) VALUES
(6, 'cocorron', 90),
(8, 'tita', 10),
(9, 'Rodesia', 100),
(11, 'Pan', 30),
(12, 'caramelos', 5),
(13, 'Factura', 50),
(14, 'churro', 80),
(17, 'medialuna', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pelis`
--

CREATE TABLE `pelis` (
  `peli_id` int(10) NOT NULL,
  `peli_nomb` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `candy`
--
ALTER TABLE `candy`
  ADD PRIMARY KEY (`candy_id`);

--
-- Indices de la tabla `pelis`
--
ALTER TABLE `pelis`
  ADD PRIMARY KEY (`peli_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `candy`
--
ALTER TABLE `candy`
  MODIFY `candy_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT de la tabla `pelis`
--
ALTER TABLE `pelis`
  MODIFY `peli_id` int(10) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
